package az.ingress.tictactoe;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.AssertionErrors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;


@SpringBootTest
class TicTacToeApplicationTests {

	TicTacToe ticTacToe =  new TicTacToe();

	@Test
	void contextLoads() {
	}

	@Test
	public void whenYOutsideBoardThenRuntimeException() {
		assertThatThrownBy(()->ticTacToe.play(2, 5))
				.isInstanceOf((RuntimeException.class));


	}


	@Test
	public void whenOccupiedThenRuntimeException() {
		ticTacToe.play(2, 1);
		assertThatThrownBy(()->ticTacToe.play(2, 1))
				.isInstanceOf((RuntimeException.class));

	}

	@Test
	public void givenFirstTurnWhenNextPlayerThenX() {
		assertThat('X').isEqualTo(ticTacToe.nextPlayer());
	}

	@Test
	public void givenLastTurnWasXWhenNextPlayerThenO() {
		ticTacToe.play(1, 1);
		assertThat('O').isEqualTo(ticTacToe.nextPlayer());
	}

	@Test
	public void whenPlayThenNoWinner() {
		String actual = ticTacToe.play(1, 1);
		assertThat("No winner").isEqualTo(actual);
		ticTacToe.printBoard();
	}

	@Test
	public void whenPlayAndWholeHorizontalLineThenWinner() {
		ticTacToe.play(1, 1); // X
		ticTacToe.play(1, 2); // O
		ticTacToe.play(2, 1); // X
		ticTacToe.play(2, 2); // O
		String actual = ticTacToe.play(3, 1); // X
		assertThat("X is the winner").isEqualTo(actual);
		ticTacToe.printBoard();
	}

	@Test
	public void whenPlayAndWholeVerticalLineThenWinner() {
		ticTacToe.play(2, 1); // X
		ticTacToe.play(1, 1); // O
		ticTacToe.play(3, 1); // X
		ticTacToe.play(1, 2); // O
		ticTacToe.play(2, 2); // X
		String actual = ticTacToe.play(1, 3); // O
		assertThat("O is the winner").isEqualTo(actual);
		ticTacToe.printBoard();
	}

	@Test
	public void whenPlayAndTopBottomDiagonalLineThenWinner() {
		ticTacToe.play(1, 1); // X
		ticTacToe.play(1, 2); // O
		ticTacToe.play(2, 2); // X
		ticTacToe.play(1, 3); // O
		String actual = ticTacToe.play(3, 3); // O
		assertThat("X is the winner").isEqualTo(actual);
		ticTacToe.printBoard();
	}

	@Test
	public void whenPlayAndBottomTopDiagonalLineThenWinner() {
		ticTacToe.play(1, 3); // X
		ticTacToe.play(1, 1); // O
		ticTacToe.play(2, 2); // X
		ticTacToe.play(1, 2); // O
		String actual = ticTacToe.play(3, 1); // O
		assertThat("X is the winner").isEqualTo(actual);
		ticTacToe.printBoard();
	}

	@Test
	public void whenAllBoxesAreFilledThenDraw() {
		ticTacToe.play(1, 1);
		ticTacToe.play(1, 2);
		ticTacToe.play(1, 3);
		ticTacToe.play(2, 1);
		ticTacToe.play(2, 3);
		ticTacToe.play(2, 2);
		ticTacToe.play(3, 1);
		ticTacToe.play(3, 3);
		String actual = ticTacToe.play(3, 2);
		assertThat("The result is draw").isEqualTo(actual);
		ticTacToe.printBoard();
	}
}
